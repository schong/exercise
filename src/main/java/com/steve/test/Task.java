package com.steve.test;

import java.util.concurrent.locks.ReentrantLock;

import static com.steve.test.Task.MAX_COUNT;
import static java.lang.String.format;
import static java.lang.Thread.currentThread;

public class Task {

    public static int MAX_COUNT = 100000;

    public static void main(String[] args) {

        ReentrantLock counterLock = new ReentrantLock(true);
        Counter counter = new Counter(0);

        CounterUpdater updater1 = new CounterUpdater(counter, counterLock);
        CounterUpdater updater2 = new CounterUpdater(counter, counterLock);
        CounterUpdater updater3 = new CounterUpdater(counter, counterLock);

        new Thread(updater1, "Thread 1").start();
        new Thread(updater2, "Thread 2").start();
        new Thread(updater3, "Thread 3").start();
    }
}

class CounterUpdater implements Runnable {

    private Counter counter;

    private ReentrantLock counterLock;

    public CounterUpdater(Counter counter, ReentrantLock counterLock) {
        this.counter = counter;
        this.counterLock = counterLock;
    }

    public void run() {
        while (true) {
            counterLock.lock();
            try {
                if (counter.getValue() > MAX_COUNT) {
                    break;
                }
                System.out.println(format("[%s] Counter value is [%d]", currentThread().getName(), counter.getValue()));

                counter.increment();
            } finally {
                counterLock.unlock();
            }

        }
    }
}

class Counter {
    private int value;

    public Counter(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public void increment() {
        value++;
    }
}




