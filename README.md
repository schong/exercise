# Java Technical Task Solution

## Build 
Prerequisites: Java and Maven beed to be installed

Change directory into project:
cd excercise

From the command line run:
mvn clean install  

This will generate an executable Jar file

## Run from command line
Whilst in the top level of the project, run the following command:

java -jar target/exercise-1.0-SNAPSHOT.jar

## Run in IDE
The Task.java contains the main method.  


 